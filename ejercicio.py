#___author___"Gabriela Viñan"
#___ email___ "gabriela.vinan@unl.edu.ec"

def numero_primo(numero):
    if numero <= 1:
        return False
    else:
        for div in range(2, numero):
          if (numero % div == 0 and div != numero):
            return False
    return True
try:
        numero = int(input("Ingresa un número: "))
        div= numero_primo(numero)
        if div is True:
                print("El número ",numero," es primo")
        else:
                print("El número  ",numero,"  no es primo")
except:
    print("\nEl numero tiene que ser entero")
